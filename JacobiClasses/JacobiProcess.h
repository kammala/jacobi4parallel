#ifndef JACOBIPROCESS_H
#define JACOBIPROCESS_H

#include <mpi.h>
#include <string>

namespace Jacobi
{

class JacobiProcess
{
private:
    //tags to Sendrecv_replace
    const static int upSendTag = 1;
    const static int dnSendTag = 2;
    const static int ltSendTag = 3;
    const static int rtSendTag = 4;

    const static int upRecvTag = 2;
    const static int dnRecvTag = 1;
    const static int ltRecvTag = 4;
    const static int rtRecvTag = 3;



protected:
    // topology
    const int rank;
    MPI::Cartcomm Rect;

    double *A;
    double *B;
    const int *BlockSize;

    int coords[2];

    // neighbours
    int up;
    int down;
    int left;
    int right;

    // shadows
    double *UpShape;
    double *DownShape;
    double *LeftShape;
    double *RightShape;

    int *dims;

    virtual double * newShape(size_t size);
    virtual void delShape(double * shape);

    virtual void updateShadowsFromLocal();
    virtual void updateLocalFromShadows();
    void updateNeighbour(bool isUpLf, bool isUpDn, int &cur);
public:
    double eps;

    JacobiProcess(const MPI::Cartcomm &comm, const int *this_dims, int big_rank, const int *blocks);
    virtual ~JacobiProcess();

    void shadowRenew();
    virtual void relax();
    virtual void resid();
    virtual double verify();

    int getUpNeighbour();
    int getDownNeighbour();
    int getLeftNeighbour();
    int getRightNeighbour();

    // debug
    virtual const std::string matrixes();
    virtual const std::string shapes();
};

}

#endif // JACOBIPROCESS_H
