#ifndef JACOBI_CUDASUBPROCESS_H
#define JACOBI_CUDASUBPROCESS_H

#include <JacobiProcess.h>
#include <cuda_runtime.h>

namespace Jacobi {

class CudaSubprocess : public JacobiProcess
{
private:
    const bool UseThrust;

    double *dev_A;
    double *dev_B;

    // shadows
    double *dev_UpShape;
    double *dev_DownShape;
    double *dev_LeftShape;
    double *dev_RightShape;

    // run configuration
    dim3 grid;
    dim3 block;

    virtual void updateShadowsFromLocal();
    virtual void updateLocalFromShadows();

    virtual double * newShape(size_t size);
    virtual void delShape(double * shape);
public:
    CudaSubprocess(const MPI::Cartcomm &comm,
                   const int *this_dims,
                   int big_rank,
                   const int *blocks,
                   const int *cuda_grid,
                   const int *cuda_block,
                   bool use_thrust
                  );
    virtual ~CudaSubprocess();

    virtual void relax();
    virtual void resid();
    virtual double verify();
    virtual const std::string matrixes();
};

} // namespace Jacobi

#endif // JACOBI_CUDASUBPROCESS_H
